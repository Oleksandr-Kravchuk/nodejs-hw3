const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const {InvalidRequestError} = require('../utils/customErrors');

const getUserTrucks = async (req, res) => {
  const {userId} = req.user;
  const trucks = await Truck.find({created_by: userId}, '-__v');

  if (!trucks) {
    throw new InvalidRequestError();
  }

  res.status(200).json({trucks: trucks});
};

const addUserTruck = async (req, res) => {
  const {userId} = req.user;
  const truck = new Truck({...req.body, created_by: userId});

  await truck.save();

  res.status(200).json({message: 'Truck created successfully'});
};

const getUserTruckById = async (req, res) => {
  const {userId} = req.user;
  const truckId = req.params.id;
  const truck = await Truck.findOne({
    _id: truckId,
    created_by: userId,
  }, '-__v');

  if (!truck) {
    throw new InvalidRequestError('No truck found');
  }

  res.status(200).json({truck});
};

const updateUserTruckById = async (req, res) => {
  const {userId} = req.user;
  const truckId = req.params.id;
  const updatedTruck = req.body;
  const truck = await Truck.findOne({
    _id: truckId,
    created_by: userId,
    status: 'IS',
  });

  if (!truck) {
    throw new InvalidRequestError('No truck found or truck can not be updated');
  }
  await truck.updateOne({$set: updatedTruck});

  res.status(200).json({message: 'Truck details changed successfully'});
};

const deleteUserTruckById = async (req, res) => {
  const {userId} = req.user;
  const truckId = req.params.id;
  const truck = await Truck.findOne({
    _id: truckId,
    created_by: userId,
    status: 'IS',
  });

  if (!truck) {
    throw new InvalidRequestError('No truck found or truck can not be deleted');
  }
  await truck.remove();

  res.status(200).json({message: 'Truck deleted successfully'});
};

const assignUserTruckById = async (req, res) => {
  const {userId} = req.user;
  const truckId = req.params.id;
  const isOnLoadTruck = await Load.find({
    assigned_to: userId,
    created_by: userId,
    status: 'OL',
  });

  if (!isOnLoadTruck) {
    throw new InvalidRequestError('The driver is on a load');
  }

  const truck = await Truck.findOne({
    _id: truckId,
    created_by: userId,
    status: 'IS',
  });

  if (!truck) {
    throw new InvalidRequestError('No truck found');
  }

  truck.assigned_to = userId;
  await truck.save();

  res.status(200).json({message: 'Truck assigned successfully'});
};

module.exports = {
  getUserTrucks,
  addUserTruck,
  getUserTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignUserTruckById,
};
