const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const {InvalidRequestError} = require('../utils/customErrors');

const TRUCKS_TYPES = [
  {
    type: 'SPRINTER',
    payload: 1700,
    dimensions: {
      width: 300,
      length: 250,
      height: 170,
    },
  },
  {
    type: 'SMALL STRAIGHT',
    payload: 2500,
    dimensions: {
      width: 500,
      length: 250,
      height: 170,
    },
  },
  {
    type: 'LARGE STRAIGHT',
    payload: 4000,
    dimensions: {
      width: 700,
      length: 350,
      height: 170,
    },
  },
];
const LOAD_STATE = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

const getUserLoads = async (req, res) => {
  const {userId, role} = req.user;
  const offset = Number(req.query.offset) || 0;
  const limit = Number(req.query.limit) || 10;
  const filterByStatus = req.query.status;
  let findLoadsObject = {};

  if (role === 'DRIVER') {
    findLoadsObject = filterByStatus ? {
      assigned_to: userId,
      status: filterByStatus,
    } : {assigned_to: userId};
  }

  findLoadsObject = filterByStatus ? {
    created_by: userId,
    status: filterByStatus,
  } : {created_by: userId};

  const loads = await Load.find(findLoadsObject, '-__v')
      .skip(offset)
      .limit(limit);

  if (!loads) {
    throw new InvalidRequestError();
  }

  res.status(200).json({loads});
};

const addUserLoad = async (req, res) => {
  const {userId} = req.user;
  const load = new Load({...req.body, created_by: userId});

  await load.save();

  res.status(200).json({message: 'Load created successfully'});
};

const getUserActiveLoad = async (req, res) => {
  const {userId} = req.user;
  const load = await Load.findOne({
    assigned_to: userId,
    status: 'ASSIGNED',
  }, '-__v');

  if (!load) {
    throw new InvalidRequestError();
  }

  res.status(200).json({load});
};

const iterateLoadState = async (req, res) => {
  const {userId} = req.user;
  const load = await Load.findOne({assigned_to: userId}, '-__v');

  if (!load) {
    throw new InvalidRequestError();
  }

  const loadStateIndex = LOAD_STATE.findIndex((state) => state === load.state);

  load.state = LOAD_STATE[loadStateIndex + 1];

  const logInfo = {
    message: load.state,
    time: Date.now(),
  };

  load.logs.push(logInfo);

  if (load.state === 'Arrived to delivery') {
    const truck = await Truck.findOne({assigned_to: userId}, '-__v');

    truck.status = 'IS';
    load.status = 'SHIPPED';
    await truck.save();
  }

  await load.save();
  res.status(200).json({message: `Load state changed to ${load.state}`});
};

const getUserLoadById = async (req, res) => {
  const {userId} = req.user;
  const loadId = req.params.id;
  const load = await Load.findOne({
    _id: loadId,
    created_by: userId,
  }, '-__v');

  if (!load) {
    throw new InvalidRequestError('No load found');
  }

  res.status(200).json({load});
};

const updateUserLoadById = async (req, res) => {
  const {userId} = req.user;
  const loadId = req.params.id;
  const updatedLoad = req.body;
  const load = await Load.findOne({
    _id: loadId,
    created_by: userId,
    status: 'NEW',
  });

  if (!load) {
    throw new InvalidRequestError('No load found or load can not be updated');
  }
  await load.updateOne({$set: updatedLoad});

  res.status(200).json({message: 'Load details changed successfully'});
};

const deleteUserLoadById = async (req, res) => {
  const {userId} = req.user;
  const loadId = req.params.id;
  const load = await Load.findOne({
    _id: loadId,
    created_by: userId,
    status: 'NEW',
  });

  if (!load) {
    throw new InvalidRequestError('No load found or load can not be deleted');
  }

  await load.remove();

  res.status(200).json({message: 'Load deleted successfully'});
};

const postUserLoadById = async (req, res) => {
  const {userId} = req.user;
  const loadId = req.params.id;
  const load = await Load.findOne({
    _id: loadId,
    created_by: userId,
    status: 'NEW',
  });

  load.status = 'POSTED';

  await load.save();

  const payload = load.payload;
  const dimensions = load.dimensions;
  const findedTruck = TRUCKS_TYPES.find((truck) => {
    if (truck.payload > payload && truck.dimensions.width > dimensions.width &&
       truck.dimensions.length > dimensions.length &&
        truck.dimensions.height > dimensions.height) {
      return truck;
    }
  });

  if (!findedTruck) {
    const logInfo = {
      message: 'No truck found',
      time: Date.now(),
    };

    load.status = 'NEW';
    load.logs.push(logInfo);
    await load.save();

    throw new InvalidRequestError('No truck found for this load');
  }

  const truckWithDriver = await Truck.where('assigned_to').ne(null)
      .findOne({type: findedTruck.type, status: 'IS'});

  if (!truckWithDriver) {
    const logInfo = {
      message: 'No driver found',
      time: Date.now(),
    };

    load.status = 'NEW';
    load.logs.push(logInfo);
    await load.save();

    throw new InvalidRequestError('No driver found for this load');
  }

  truckWithDriver.status = 'OL';
  await truckWithDriver.save();

  const logInfo = {
    message: `Load assigned to driver with id ${truckWithDriver.assigned_to} and
      load state changed to 'En route to Pick Up'`,
    time: Date.now(),
  };

  load.logs.push(logInfo);
  load.assigned_to = truckWithDriver.assigned_to;
  load.status = 'ASSIGNED';
  load.state = 'En route to Pick Up';
  await load.save();

  res.status(200).json({
    message: 'Load posted successfully',
    driver_found: true,
  });
};

const getUserLoadShippingDetailsById = async (req, res) => {
  const {userId} = req.user;
  const loadId = req.params.id;
  const load = await Load.where('status').ne('SHIPPED')
      .findOne({_id: loadId, created_by: userId}, '-__v');

  if (!load) {
    throw new InvalidRequestError('No load found with such id');
  }

  const truck = await Truck.findOne({assigned_to: load.assigned_to}, '-__v');

  res.status(200).json(truck ? {'load': load, 'truck': truck} : {load});
};

module.exports = {
  getUserLoads,
  addUserLoad,
  getUserActiveLoad,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingDetailsById,
};
