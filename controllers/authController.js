const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {User} = require('../models/userModel');
const {
  InvalidCredentialsError,
  InvalidEmailForForgotPasswordError,
} = require('../utils/customErrors');
const JWT_SECRET = 'secret';

const registerUser = async (req, res) => {
  const {
    email,
    password,
    role,
  } = req.body;

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();
  res.status(200).json({message: 'Profile created successfully'});
};

const loginUser = async (req, res) => {
  const {
    email,
    password,
  } = req.body;

  const user = await User.findOne({email});

  if (!user) {
    throw new InvalidCredentialsError();
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new InvalidCredentialsError();
  }

  const token = jwt.sign({
    _id: user._id,
    email: user.email,
    role: user.role,
  }, JWT_SECRET);

  res.status(200).json({
    jwt_token: token,
  });
};

const forgotUserPassword = async (req, res) => {
  const {
    email,
  } = req.body;

  const user = await User.findOne({email});

  if (!user) {
    throw new InvalidEmailForForgotPasswordError();
  }

  res.status(200).json({
    message: 'New password sent to your email address',
  });
};

module.exports = {
  registerUser,
  loginUser,
  forgotUserPassword,
};
