/* eslint-disable require-jsdoc */
class CustomError extends Error {
  constructor(message) {
    super(message);
    this.status = 500;
  }
}

class InvalidRequestError extends CustomError {
  constructor(message = 'Invalid request') {
    super(message);
    this.status = 400;
  }
}

class InvalidUserRoleError extends CustomError {
  constructor(message = 'Invalid user role') {
    super(message);
    this.status = 400;
  }
}

class InvalidCredentialsError extends CustomError {
  constructor(message = 'Invalid password or email') {
    super(message);
    this.status = 400;
  }
}

class InvalidEmailForForgotPasswordError extends CustomError {
  constructor(message = 'Invalid email') {
    super(message);
    this.status = 400;
  }
}

class RegisterError extends CustomError {
  constructor(message = 'Invalid request') {
    super(message);
    this.status = 401;
  }
}

module.exports = {
  CustomError,
  InvalidRequestError,
  InvalidUserRoleError,
  InvalidCredentialsError,
  InvalidEmailForForgotPasswordError,
  RegisterError,
};
