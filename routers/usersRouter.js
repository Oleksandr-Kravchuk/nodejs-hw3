const express = require('express');
const router = new express.Router();
const {
  getUser,
  deleteUser,
  changeUserPassword,
} = require('../controllers/usersController');
const {asyncWrapper} = require('../utils/asyncWrapper');

router.get('/', asyncWrapper(getUser));
router.delete('/', asyncWrapper(deleteUser));
router.patch('/password', asyncWrapper(changeUserPassword));

module.exports = {
  usersRouter: router,
};
