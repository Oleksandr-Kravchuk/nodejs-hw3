const express = require('express');
const router = new express.Router();
const {
  getUserTrucks,
  addUserTruck,
  getUserTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignUserTruckById,
} = require('../controllers/trucksController');
const {asyncWrapper} = require('../utils/asyncWrapper');
const {truckValidator} = require('../middlewares/validationMiddleware');
const {isDriverMiddleware} = require('../middlewares/roleMiddleware');

router.get('/', isDriverMiddleware, asyncWrapper(getUserTrucks));
router.post('/',
    truckValidator,
    isDriverMiddleware,
    asyncWrapper(addUserTruck),
);
router.get('/:id', isDriverMiddleware, asyncWrapper(getUserTruckById));
router.put('/:id', isDriverMiddleware, asyncWrapper(updateUserTruckById));
router.delete('/:id', isDriverMiddleware, asyncWrapper(deleteUserTruckById));
router.post('/:id/assign',
    isDriverMiddleware,
    asyncWrapper(assignUserTruckById),
);

module.exports = {
  trucksRouter: router,
};
