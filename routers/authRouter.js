const express = require('express');
const router = new express.Router();
const {
  registerUser,
  loginUser,
  forgotUserPassword,
} = require('../controllers/authController');
const {asyncWrapper} = require('../utils/asyncWrapper');
const {registrationValidator} = require('../middlewares/validationMiddleware');

router.post('/register', registrationValidator, asyncWrapper(registerUser));
router.post('/login', registrationValidator, asyncWrapper(loginUser));
router.post('/forgot_password',
    registrationValidator,
    asyncWrapper(forgotUserPassword),
);

module.exports = {
  authRouter: router,
};
