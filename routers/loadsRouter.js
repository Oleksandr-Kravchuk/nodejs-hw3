const express = require('express');
const router = new express.Router();
const {
  getUserLoads,
  addUserLoad,
  getUserActiveLoad,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingDetailsById,
} = require('../controllers/loadsController');
const {asyncWrapper} = require('../utils/asyncWrapper');
const {loadValidator} = require('../middlewares/validationMiddleware');
const {
  isDriverMiddleware,
  isShipperMiddleware,
} = require('../middlewares/roleMiddleware');

router.get('/', asyncWrapper(getUserLoads));
router.post('/', loadValidator, isShipperMiddleware, asyncWrapper(addUserLoad));
router.get('/active', isDriverMiddleware, asyncWrapper(getUserActiveLoad));
router.patch('/active/state',
    isDriverMiddleware,
    asyncWrapper(iterateLoadState),
);
router.get('/:id', isShipperMiddleware, asyncWrapper(getUserLoadById));
router.put('/:id', isShipperMiddleware, asyncWrapper(updateUserLoadById));
router.delete('/:id', isShipperMiddleware, asyncWrapper(deleteUserLoadById));
router.post('/:id/post', isShipperMiddleware, asyncWrapper(postUserLoadById));
router.get('/:id/shipping_info',
    isShipperMiddleware,
    asyncWrapper(getUserLoadShippingDetailsById),
);

module.exports = {
  loadsRouter: router,
};
