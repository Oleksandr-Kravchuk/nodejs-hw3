const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: String,
    require: true,
    default: null,
  },
  type: {
    type: String,
    require: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    require: true,
    default: 'IS',
    enum: ['IS', 'OL'],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {
  Truck,
};
