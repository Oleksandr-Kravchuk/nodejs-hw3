const {InvalidUserRoleError} = require('../utils/customErrors');

const isDriverMiddleware = async (req, res, next) => {
  const {role} = req.user;

  if (role === 'SHIPPER') {
    throw new InvalidUserRoleError('The user is not a driver');
  }

  next();
};

const isShipperMiddleware = async (req, res, next) => {
  const {role} = req.user;

  if (role === 'DRIVER') {
    throw new InvalidUserRoleError('The user is not a shipper');
  }

  next();
};

module.exports = {
  isDriverMiddleware,
  isShipperMiddleware,
};
